const express = require('express');
const router = express.Router();

const axios = require('axios');
const moment = require('moment');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'pug-Bootstrap' });
});

router.get('/dashboard', function(req, res, next) {
  res.render('dashboard', { title: 'pug-Bootstrap' });
});


router.get('/login', function(req, res, next) {
  res.render('sign-in', { 
	title: 'pug-Bootstrap',
	data: {},
	errors: {}
  });
});
router.post('/login', function(req, res, next) {
	//console.log(req);
	console.log(req.body.inputEmail);
	console.log(req.body.inputPassword);
	console.log(req.body.inputTeam);
  res.render('dashboard', { title: 'pug-Bootstrap' });
});


router.get('/team/:teamName/stories/links', getLinks);


router.get('/team/:teamName/sprints', function(req, res, next) {
	let teamName = req.params.teamName;
	axios.get('http://localhost:3000/team/'+teamName+'/sprints')
	.then(function(response) {
			const sprints = response.data;
			const title = "Team Sid sprints";
			res.render('sprints', { title: title, sprints: sprints, teamName: teamName });

	})
	.catch(error => {
		console.log(error)
	})
  
});

router.get('/team/:teamName/sprints/full', function(req, res, next) {
	let teamName = req.params.teamName;
	axios.get('http://localhost:3000/team/'+teamName+'/sprints/fullDetails')
	.then(function(response) {
			const sprints = response.data;
			const title = "Team "+teamName+" sprints";
			res.render('fullSprints', { title: title, sprints: sprints, teamName: teamName });

	})
	.catch(error => {
		console.log(error)
	})
  
});

router.get('/team/:teamName/sprint/:sprintId/stories', function(req, res, next) {
	var sprintId = req.params.sprintId;
	let teamName = req.params.teamName
	var url = 'http://localhost:3000/team/'+teamName+'/'+sprintId+'/stories';
	console.log(url);
	axios.get(url)
	.then(function(response) {
			const sprint = response.data;
			const pbis = sprint.pbis;
			const title = "Team Sid story details "+sprint.name;
			res.render('table2', { title: title, stories: pbis });

	})
	.catch(error => {
		console.log(error)
	})
});

router.get('/team/:teamName/sprint/:sprintId/sprintReport', showSprintReport)

router.get('/team/:teamName/sprint/:sprintId/storySummary', function(req, res, next) {
	var sprintId = req.params.sprintId;
	let teamName = req.params.teamName
	var url = 'http://localhost:3000/team/'+teamName+'/'+sprintId+'/stories';
	console.log(url);
	axios.get(url)
	.then(function(response) {
			const sprint = response.data;
			const title = "Team Sid story summaries for "+sprint.name;
			res.render('sprint_pbis_min', { title: title, stories: sprint.pbis });

	})
	.catch(error => {
		console.log(error)
	})
  
});


router.get('/Sid/missingFixVersions', fixVersions );


module.exports = router;


function showSprintReport(req, res, next) {
	var sprintId = req.params.sprintId;
	let teamName = req.params.teamName
	var url = 'http://localhost:3000/team/'+teamName+'/'+sprintId+'/stories';
	console.log(url);
	axios.get(url)
	.then(function(response) {
			const sprint = response.data;
			const pbis = sprint.pbis;
			const totals = sprint.totals;


	//console.log(arrCompletedStoryPts);
			const title = "Sprint Report for "+sprint.name;
			res.render('sprintReport', { title: title, stories: pbis, totals:totals });

	})
	.catch(error => {
		console.log(error)
	})
}

function fixVersions(req, res, next) {
	var sprintId = req.params.sprintId;
	let teamName = req.params.teamName;
	var url = 'http://localhost:3000/team/Sid/stories';
	console.log(url);
	axios.get(url)
	.then(function(response) {
		const pbis = response.data;
		var filteredPbis = [];
		for (const pbi of pbis) {
			if (pbi.fixVersion == "") {
				filteredPbis.push(pbi);
			}
		}
		return filteredPbis;
	})
	.then(function(pbis) {
		const title = "test for Team Sid sprints from the Backlog";
		res.render('SidMissingFixVersions', { title: title, stories: pbis });

	})
	.catch(error => {
		console.log(error)
	})
  
}

function getLinks(req, res, next) {
	let teamName = req.params.teamName;
	axios.get('http://localhost:3000/team/'+teamName+'/stories')
	.then(function(response) {
			const stories = response.data;
			const title = "test for Team Sid Stories from the Backlog";
			res.render('table', { title: title, stories: stories });

	})
	.catch(error => {
		console.log(error)
	})
  
}


